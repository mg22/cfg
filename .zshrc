# Path to your oh-my-zsh installation.
export ZSH=${HOME}/.oh-my-zsh
export ZSH_CUSTOM=${HOME}/.oh-my-zsh-custom/

ZSH_THEME="emgee"
# ZSH_THEME="robbyrussell"
# ZSH_THEME="agnoster"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(
    git-prompt
    command-not-found
    zsh-autosuggestions
)

source $ZSH/oh-my-zsh.sh

source $ZSH_CUSTOM/aliases
source $ZSH_CUSTOM/exports

# Load local zshrc.
if [ -f "${HOME}/.zshrc-local" ]; then
    source "${HOME}/.zshrc-local"
fi
