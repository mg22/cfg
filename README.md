# MG's dotfiles

Repo for all my dotfiles.

## What's inside

* tmux
	* more sane hotkeys
	* small tweaks to colors
	* tab ordering from 1
* zshrc
	* custom lightweight theme
	* few useful plugins, zsh-autosuggestions
	* few shortcuts and aliases for common commands
	* $HOME/.local hierarchy added to PATH
* fish
    * monokai theme
    * "Informative" default prompt with git prompt shoved in
    * few shortcuts and aliases, $HOME/.local hierarchy added to path
* gitconfig
	* aliases for common commands
* nvim
	* few basic tweaks (nowrap, tabs, numbers, vnoremap, ...), mostly taken from old .vimrc
	* monokai theme
	* few useful plugins
	* a list of language specific plugins, commented out and untested
* vimrc
	* old one, may be obsolete, mostly just for inspiration

## Installation

* clone this repo with submodules (`git clone --recurse-submodules`)
* install usefull packages: `$ ./scripts/install_<your_distribution>.sh` (currently just ubuntu22)
* symlink dotfiles to home: `$ ./scripts/bootstrap.sh`


## Additional inspiration
- https://github.com/FloopCZ/linux-environment
- https://github.com/Teyras/dotfiles
- https://github.com/pin3da/dotfiles
