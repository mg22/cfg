call plug#begin('~/.local/share/nvim/plugged')

"""""""""""""""""""""""""""
" Generally usefull plugins
"""""""""""""""""""""""""""

" Jump from .h to .cpp and vice versa
Plug 'vim-scripts/a.vim'

" Multiple cursors
Plug 'terryma/vim-multiple-cursors'

" Nerdtree
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'

" Underline word under cusor
Plug 'itchyny/vim-cursorword'

" Colorschemes
Plug 'rafi/awesome-vim-colorschemes'

" Generate doxygen comment templates
Plug 'vim-scripts/DoxygenToolkit.vim'

" Comment out lines of code
Plug 'tpope/vim-commentary'

" Gitgutter: display git diff info
Plug 'airblade/vim-gitgutter'

" Custom monokai theme.
Plug 'tanvirtin/monokai.nvim'

"""""""""""""""""""""""""""""""""""""
" Language/toolchain specific plugins
"
" all are off by default, uncommment
" only ones you need, as they most
" likely require additional setup
"""""""""""""""""""""""""""""""""""""

" Asynchronous lint engine
"Plug 'W0rp/ale'

" Rust language support
"Plug 'rust-lang/rust.vim'

" Clang code complete
"Plug 'rhysd/vim-clang-format'
"Plug 'roxma/nvim-completion-manager'
"Plug 'roxma/ncm-clang'

" Javascript
"Plug 'pangloss/vim-javascript'
"Plug 'mxw/vim-jsx'

" Clojure
"Plug 'tpope/vim-fireplace'
"Plug 'guns/vim-clojure-static'
"Plug 'guns/vim-clojure-highlight'

call plug#end()


""""" Basic stuff
set nowrap
set number
set autowrite
set tags=./tags,tags;$HOME
set colorcolumn=80,110

"" Indentation
set expandtab
set tabstop=4
set shiftwidth=4

"" code folding
set foldmethod=syntax
set foldlevel=20

" always use '+' and '*' clipboards for all operations
set clipboard+=unnamedplus

" reselect visual block after indent
vnoremap < <gv
vnoremap > >gv

" wait time until swap is written to disk
" impacts refresh of gutter like git diff or clang-check
set updatetime=200

""""" Themes
" (probably) needed for xterm-256color mode
set background=dark
colorscheme monokai

""""" Nerdtree & stuff
"" [nerdtreetabs]
" toggle nerdtree
map <silent> <F2> :NERDTreeMirrorToggle<CR>
" toggle nerdtree in all tabs
map <silent> <F3> :NERDTreeTabsToggle<CR>

""""" Additional local settings
if !empty(glob('~/.config/nvim/init.vim.local'))
    source ~/.config/nvim/init.vim.local
endif
