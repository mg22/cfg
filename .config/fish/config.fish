if status is-interactive
    # Commands to run in interactive sessions can go here.

    # A bunch of useful aliases.
    alias v="nvim"
    alias c="cd"
    alias pud="pushd"
    alias pod="popd"
    alias l="la"
end

# Make Neovim the default editor.
set -gx VISUAL 'nvim'
set -gx EDITOR $VISUAL

# Add user's home/.local hierarchy to appropriate variables.
set -gx PATH "$HOME/.local/bin:$PATH"
set -gx LIBRARY_PATH "$HOME/.local/lib:$LIBRARY_PATH"
set -gx LD_LIBRARY_PATH "$HOME/.local/lib:$LD_LIBRARY_PATH"
set -gx CPLUS_INCLUDE_PATH "$HOME/.local/include:$CPLUS_INCLUDE_PATH"
