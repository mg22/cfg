local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"

PROMPT='%{$fg_bold[blue]%}${PWD/#$HOME/~}%{$reset_color%}$(git_super_status) 
${ret_status}%{$reset_color%}'

RPROMPT="[%{$fg[yellow]%}%?%{$reset_color%}]"

ZSH_THEME_GIT_PROMPT_PREFIX=" git:("
