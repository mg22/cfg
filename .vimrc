execute pathogen#infect()

""""" Basic stuff
syntax on
set nowrap
set autoindent
set number
set autowrite
"set fileencodings=ucs-bom,utf-8,cp1250,iso-8859-2,default
set tags=./tags,tags;$HOME
set colorcolumn=100
set hlsearch

"" indentation
set shiftwidth=4
set tabstop=4
set expandtab

"" code folding
set foldmethod=syntax
set foldlevel=20

" system clipboard for y and d
set clipboard=unnamed,unnamedplus
" reselect visual block after indent
vnoremap < <gv
vnoremap > >gv

""""" Endable filetype specific settings
filetype plugin indent on

""""" Themes
" (probably) needed for xterm-256color mode
set t_ut=
set background=dark
"colorscheme solarized
colorscheme monokai
"colorscheme molokai
"let g:molokai_original=1
"colorscheme solarized 

""""" Per project .vimrc settings
"set exrc

""""" Nerdtree & stuff
"" [nerdtreetabs]
" toggle nerdtree
map <silent> <F2> :NERDTreeMirrorToggle<CR>
" toggle nerdtree in all tabs
map <silent> <F3> :NERDTreeTabsToggle<CR>

""""" Javascript
"" [vim-jsx]
" enable highlight of jsx in .js files
let g:jsx_ext_required = 0

" Disable unsafe commands in project-specific .vimrc files
"set secure

""""" Machine specific settings
" loaded from ~/.vimrc.local
if filereadable(glob("~/.vimrc.local"))
  source ~/.vimrc.local
endif
