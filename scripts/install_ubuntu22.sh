#!/usr/bin/env bash

echo 'INFO: installing usefull packages'
sudo apt-get install curl tmux neovim zsh

echo 'INFO: installing oh-my-zsh'
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo 'INFO: installing plug (nvim plugin manager)'
# install nvim plug manager
if [ ! -f "$HOME/.local/share/nvim/site/autoload/plug.vim" ]; then
    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    nvim '+PlugUpdate' '+PlugClean!' '+PlugUpdate' '+qall'
fi

echo 'INFO: installing fish shell'
sudo apt-add-repository ppa:fish-shell/release-3
sudo apt update
sudo apt install fish
