#!/usr/bin/env python3

import argparse
from pathlib import Path
import logging
import os

ROOT_FOLDER = Path(__file__).resolve().parent.parent

DOTFILES = [
    ".gitconfig",
    ".tmux.conf",
    ".zshrc",
    ".vimrc",
    ".config/nvim",
    ".config/fish/config.fish",
    ".config/fish/functions/fish_prompt.fish",
    ".config/fish/themes/monokai.theme",
    ".oh-my-zsh-custom",
]


def backup_path(path):
    tries = 100
    for i in range(tries):
        backup = path.with_name(f"{path.name}.backup{str(i) if i > 0 else ''}")
        logging.debug(f"Trying to back up {path} to {backup}.")
        if backup.exists():
            logging.debug(
                f"Backup {backup} already exists, trying with larger suffix number."
            )
            continue
        os.rename(path, backup)
        logging.debug(f"Success, {path} backed to {backup}.")
        return True, backup

    logging.warning(f"Giving up creating backup after {tries} tries.")
    return False, None


def install():
    logging.debug("Creating symlinks.")

    for dotfile in DOTFILES:
        file_path = ROOT_FOLDER / dotfile
        symlink_path = Path.home() / dotfile

        logging.info(f"Symlinking {file_path} -> {symlink_path}.")

        # Check if to be created symlink already exists.
        if symlink_path.exists():
            logging.debug(f"Path {symlink_path} already exists, trying to back it up.")

            # Try to back up exisiting file/folder.
            success, backed_path = backup_path(symlink_path)
            if success:
                logging.info(
                    f"{symlink_path} already exists, backed up existing file to {backed_path}"
                )
            else:
                logging.warning(
                    f"Cannot back up {symlink_path}, skipping creating symlink."
                )
                continue

        # Create parent folders in case of longer path and they don't exist.
        if len(Path(dotfile).parts) > 1:
            logging.debug(f"Trying to create parent folders for path {symlink_path}")
            symlink_path.parent.mkdir(parents=True, exist_ok=True)

        try:
            symlink_path.symlink_to(file_path)
        except FileExistsError as e:
            logging.warning(f"Failed to create symlink for {dotfile}, {e}. Skipping...")

    logging.debug("Done.")


def uninstall():
    logging.debug("Removing symlinks.")

    for dotfile in DOTFILES:
        symlink_path = Path.home() / dotfile

        if not symlink_path.exists():
            logging.info(f"File {symlink_path} does not exist, skipping.")
            continue

        if not symlink_path.is_symlink():
            logging.warning(f"File {symlink_path} is not a symlink, skipping.")
            continue

        try:
            symlink_path.unlink()
            logging.info(f"Symlink {symlink_path} unlinked.")
        except FileNotFoundError:
            logging.warning(f"File {symlink_path} does not exist, skipping.")

    logging.debug("Done.")


def main():
    parser = argparse.ArgumentParser(description="Bootstrap dotfiles to home folder.")
    parser.add_argument(
        "--uninstall",
        action="store_true",
        help="Remove symlinks from home folder instead.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="Increase verbosity level.",
    )

    args = parser.parse_args()

    log_level = logging.INFO
    if args.verbose:
        log_level = logging.DEBUG
    logging.basicConfig(format="%(levelname)-8s %(message)s", level=log_level)

    if args.uninstall:
        uninstall()
    else:
        install()


if __name__ == "__main__":
    main()
